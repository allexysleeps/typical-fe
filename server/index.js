const express = require('express')
const cors = require('cors')

const data = require('./data.json')

const pick = (fields) => (data) => fields.reduce((obj, f) => ({ ...obj, [f]: data[f] }), {})

express()
  .use(cors())
  .get('/search', (req, res) => {
    res.send(data.map(pick([
      'name',
      '_id',
      'company',
      'email',
      'phone',
      'age'
    ])))
  })
  .get('/item/:id', (req, res) => {
    const { id } = req.params
    const item = data.find((i) => i._id === id)
    if (item) {
      res.send(item)
    } else {
      res.sendStatus(404)
    }
  })
  .listen(3000, () => console.log('Server is listening to localhost:3000'))
