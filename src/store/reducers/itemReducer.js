import { REQUEST_STATUS } from "../../constants";
import { put, takeEvery, call } from 'redux-saga/effects'
import {getItem} from '../../api'

const ITEM_REQUEST = "ITEM/REQUEST";
const ITEM_SUCCESS = "ITEM/SUCCESS";
const ITEM_FAILURE = "ITEM/FAILURE";

export const itemRequest = (payload) => ({ type: ITEM_REQUEST, payload: { id: payload } });
export const itemSuccess = payload => ({ type: ITEM_SUCCESS, payload });
export const itemFailure = () => ({ type: ITEM_FAILURE });

export const item = (
  state = {
    itemData: {},
    requestStatus: REQUEST_STATUS.NOT_ASKED
  },
  { type, payload }
) => {
  switch (type) {
    case ITEM_REQUEST: {
      return { ...state, requestStatus: REQUEST_STATUS.PENDING };
    }
    case ITEM_SUCCESS: {
      return {
        ...state,
        requestStatus: REQUEST_STATUS.SUCCESS,
        itemData: payload
      };
    }
    case ITEM_FAILURE: {
      return { ...state, requestStatus: REQUEST_STATUS.FAILURE, itemData: {} };
    }
  }
  return state;
};


export function *getItemDataFromServer({ payload }) {
  const { id } = payload
  const data = yield call(getItem, id)
  if (data) {
    yield put(itemSuccess(data))
  } else {
    yield put(itemFailure())
  }
}

export function *watchGetItemReq() {
  yield takeEvery(ITEM_REQUEST, getItemDataFromServer)
}