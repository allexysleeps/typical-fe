import {REQUEST_STATUS} from '../../constants'

const ITEM_LIST_REQUEST = 'ITEM_LIST/REQUEST'
const ITEM_LIST_SUCCESS = 'ITEM_LIST/SUCCESS'
const ITEM_LIST_FAILURE = 'ITEM_LIST/FAILURE'

export const itemListRequest = () => ({ type: ITEM_LIST_REQUEST })
export const itemListSuccess = (payload) => ({ type: ITEM_LIST_SUCCESS, payload })
export const itemListFailure = () => ({ type: ITEM_LIST_FAILURE })

export const itemList = (state = {
  items: [],
  requestStatus: REQUEST_STATUS.NOT_ASKED
}, { type, payload}) => {
  switch (type) {
    case ITEM_LIST_REQUEST: {
      return {...state, requestStatus: REQUEST_STATUS.PENDING }
    }
    case ITEM_LIST_SUCCESS: {
      return { ...state, requestStatus: REQUEST_STATUS.SUCCESS, items: payload }
    }
    case ITEM_LIST_FAILURE: {
      return { ...state, requestStatus: REQUEST_STATUS.FAILURE, items: [] }
    }
  }
  return state
}