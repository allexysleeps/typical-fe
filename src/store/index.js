import { createStore, combineReducers, applyMiddleware } from "redux";
import { itemList } from "./reducers/itemListReducer";
import { item } from "./reducers/itemReducer";
import createSagaMiddleware from 'redux-saga'
import { all } from 'redux-saga/effects'
import { watchGetItemReq } from './reducers/itemReducer'


function *rootSaga(){
  yield all([
    watchGetItemReq()
  ])
}

const sagaMiddleware = createSagaMiddleware()

const rootReducer = combineReducers({
  itemList,
  item
});

export const store = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga)