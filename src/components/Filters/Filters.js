import React from 'react'

export const Filters = ({filterData, onFilterChange}) => (
  <div>{
    filterData.map(({key, isActive}) => (
      <div key={key}>
        <span>{key} </span>
        <input
          type="checkbox"
          checked={isActive}
          onChange={() => onFilterChange({key, isActive: !isActive})}/>
      </div>
    ))}
  </div>
)