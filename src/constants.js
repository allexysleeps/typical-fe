export const REQUEST_STATUS = {
  NOT_ASKED: 'NOT_ASKED',
  PENDING: 'PENDING',
  SUCCESS: 'SUCCESS',
  FAILURE: 'FAILURE'
}

export const FILTER_LIST = [
  'ne_bit',
  'ne_krashen',
  'rastamozshen',
]