import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter} from 'react-router-dom'
import {Provider} from 'react-redux'
import {store} from './store'
import {Root} from './pages/Root/Root'

const App = () => (
  <BrowserRouter>
    <Provider store={store}>
      <Root />
    </Provider>
  </BrowserRouter>
)

const node = document.createElement('div')
document.body.appendChild(node)

ReactDOM.render(
  <App/>,
  node
)