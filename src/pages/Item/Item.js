import React from "react";
import { connect } from "react-redux";
import { itemRequest } from "../../store/reducers/itemReducer";
import {REQUEST_STATUS} from '../../constants'

export class ItemC extends React.Component {
  componentDidMount() {
    const { match, getItemRequest } = this.props
    getItemRequest(match.params.id)
  }
  render() {
    const { requestStatus, itemData } = this.props;
    if (
      requestStatus === REQUEST_STATUS.NOT_ASKED ||
      requestStatus === REQUEST_STATUS.PENDING
    ) {
      return <h1>Loading</h1>;
    }
    if (requestStatus === REQUEST_STATUS.FAILURE) {
      return <h1>ERROR</h1>;
    }
  
    return <h1>{JSON.stringify(itemData)}</h1>;
  }
}

export const Item = connect(
  store => ({
    itemData: store.item.itemData,
    requestStatus: store.item.requestStatus
  }),
  dispatch => ({
    getItemRequest: (payload) => dispatch(itemRequest(payload)),
  })
)(ItemC);
