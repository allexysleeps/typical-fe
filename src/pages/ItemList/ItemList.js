import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom'
import qs from 'qs'
import {FILTER_LIST, REQUEST_STATUS} from '../../constants';
import {getItems} from '../../api';
import {itemListFailure, itemListRequest, itemListSuccess} from '../../store/reducers/itemListReducer';

import styled from 'styled-components';
import {Filters} from '../../components/Filters/Filters'

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
`

const Left = styled.div`

`

const Right = styled.div`

`

const Item = styled(Link)`
  border-bottom: 1px solid #333;
  display: block;
  &:nth-child(odd) {
    background: rgba(0, 0, 0, 0.5);
  }
`

const parseQs = (search) => qs.parse(search, { ignoreQueryPrefix: true })

export class ItemListC extends React.Component {
  componentDidMount() {
    this.getItemsData()
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.location.search !== this.props.location.search) {
      this.getItemsData()
    }
  }
  
  getItemsData() {
    this.props.getItemsRequest();
    getItems()
      .then(data => {
        this.props.getItemsSuccess(data);
      })
      .catch(err => {
        console.log(err)
        this.props.getItemsFailure();
      });
  }
  
  onFilterChangeHandler({ key, isActive }) {
    const { location, history } = this.props
    const oldQs = parseQs(location.search)
    const newQs = {...oldQs, [key]: isActive }
    history.push({
      pathname: location.pathname,
      search: qs.stringify(newQs)
    })
  }
  
  render() {
    const {requestStatus, items, location} = this.props;
    if (
      requestStatus === REQUEST_STATUS.NOT_ASKED ||
      requestStatus === REQUEST_STATUS.PENDING
    ) {
      return <h1>Loading</h1>;
    }
    if (requestStatus === REQUEST_STATUS.FAILURE) {
      return <h1>ERROR</h1>;
    }
    
    const filterDataObj = parseQs(location.search)
    
    const filterData = FILTER_LIST.map((key) => ({
      key,
      isActive: filterDataObj[key] === 'true'
    }))
    
    console.log({filterDataObj})
    
    return (
      <Wrapper>
        <Left>
          <Filters filterData={filterData} onFilterChange={(opt) => this.onFilterChangeHandler(opt)}/>
        </Left>
        <Right>
          {
            items.map(item => (
              <Item
                styles="border: 1px solid #333"
                key={item._id}
                to={`/item/${item._id}`}
      
              >
                <h3>{item.name}</h3>
                <p>Company: {item.company}</p>
                <p>E-Mail: {item.email}</p>
                <p>Phone: {item.phone}</p>
              </Item>
            ))
          }
        </Right>
      </Wrapper>
    )
  }
}

export const ItemList = connect(
  store => ({
    items: store.itemList.items,
    requestStatus: store.itemList.requestStatus
  }),
  dispatch => ({
    getItemsRequest: () => dispatch(itemListRequest()),
    getItemsSuccess: payload => dispatch(itemListSuccess(payload)),
    getItemsFailure: () => dispatch(itemListFailure())
  })
)(ItemListC);
